window.addEventListener('load', function()
{
    var prev = this.document.querySelector('.prev');
    var next = this.document.querySelector('.next');
    var banners = this.document.querySelector('.banners');
    setBannerTitle(0);

    banners.addEventListener('mouseenter', function()
    {
        prev.style.display = 'block';
        next.style.display = 'block';
        clearInterval(timer);
        timer = null;
    });

    banners.addEventListener('mouseleave', function()
    {
        prev.style.display = 'none';
        next.style.display = 'none';
        timer = setInterval(function()
        {
            next.click();
        }, 2000);
    });

    var images = this.document.querySelector('.images');
    var dots = this.document.querySelector('.dots');
    var bannerWidth = banners.offsetWidth;
    for(var i = 0; i < images.children.length; ++i)
    {
        var li = this.document.createElement('li');
        li.setAttribute('index', i);
        dots.appendChild(li);
        li.addEventListener('click', function()
        {
            for(var i = 0; i < dots.children.length; ++i)
            {
                dots.children[i].className = '';
            }
            this.className = 'active';

            var index = this.getAttribute('index');
            num = index;
            circle = index;
            animate(images, -index * bannerWidth);
            setBannerTitle(circle);
        });
    }

    dots.children[0].className = 'active';
    var first = images.children[0].cloneNode(true);
    images.appendChild(first);

    var num = 0;
    var circle = 0;
    next.addEventListener('click', function()
    {
        if(num == images.children.length - 1)
        {
            images.style.left = 0;
            num = 0;
        }
        ++num;
        animate(images, -num * bannerWidth);
        ++circle;
        if(circle == dots.children.length)
        {
            circle = 0;
        }
        circleChange();
        setBannerTitle(circle);
    });

    prev.addEventListener('click', function()
    {
        if(num == 0)
        {
            num = images.children.length - 1;
            images.style.left = -num * bannerWidth + 'px';
        }
        --num;
        animate(images, -num * bannerWidth);
        --circle;
        circle = circle < 0 ? dots.children.length - 1 : circle;
        circleChange();
        setBannerTitle(circle);
    });

    function circleChange()
    {
        for(var i = 0; i < dots.children.length; ++i)
        {
            dots.children[i].className = '';
        }
        dots.children[circle].className = 'active';
    }

    var timer = setInterval(function()
    {
        next.click();
    }, 2000);
});

function animate(obj, target, callback)
{
    clearInterval(obj.timer);
    obj.timer = setInterval(function()
    {
        var step = (target - obj.offsetLeft) / 10;
        step = step > 0 ? Math.ceil(step) : Math.floor(step);
        if(obj.offsetLeft == target)
        {
            clearInterval(obj.timer);
            if(callback)
            {
                callback();
            }
        }

        obj.style.left = obj.offsetLeft + step + 'px';
    }, 15);
}

function setBannerTitle(num)
{
    title = [
        "习近平同赞比亚总统希奇莱马会谈",
        "习近平会见柬埔寨首相洪玛奈",
        "总书记的一周（9月11日——9月17日）", 
        "总书记黑龙江之行“推动东北全面振兴”", 
        "习近平视察78集团军"
    ];
    document.getElementById('bannerContent').innerHTML = title[num];
}